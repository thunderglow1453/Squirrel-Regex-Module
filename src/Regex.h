#pragma once

#include <regex>

#include "sqrat/sqratArray.h"
#include "sqrat/sqratTable.h"

namespace regex
{
	class Regex
	{
	public:
		Regex(const char* pattern);

		static void bind();

		SQBool match(const char* str) const;
		static SQRESULT sq_capture(HSQUIRRELVM vm);
		static SQRESULT sq_search(HSQUIRRELVM vm);

	private:
		std::regex reg;
	};
}
