#include "Regex.h"

#include "sqrat/sqratClass.h"
#include "sqrat/sqratTable.h"

namespace regex
{
	Regex::Regex(const char* pattern) : reg(std::string(pattern, std::strlen(pattern)))
	{
	}

	void Regex::bind()
	{
		Sqrat::Class<Regex> regex(SqModule::vm, std::string("Regex"));

		regex.Ctor<const char*>();
		regex.Func("match", &match);
		regex.SquirrelFunc("capture", &sq_capture, -2, ".si");
		regex.SquirrelFunc("search", &sq_search, -2, ".si");

		Sqrat::RootTable(SqModule::vm).Bind("Regex", regex);
	}

	SQRESULT Regex::sq_capture(HSQUIRRELVM vm)
	{
		const auto top = sq_gettop(vm);
		if (top > 3)
			return sq_throwerror(vm, "wrong number of parameters");

		if (!Sqrat::ClassType<Regex>::IsValidInstance(vm, 1))
			return sq_throwerror(vm, "invalid this");

		const auto self = Sqrat::ClassType<Regex>::GetInstance(vm, 1);

		const SQChar* str;
		sq_getstring(vm, 2, &str);

		SQInteger startIdx = 0;
		if (top == 3) sq_getinteger(vm, 3, &startIdx);

		const auto strLen = std::strlen(str);
		if (static_cast<size_t>(startIdx) >= strLen)
		{
			sq_pushnull(vm);
			return 1;
		}
		const auto begin = std::cregex_iterator(str + startIdx, str + strLen, self->reg);
		const auto end = std::cregex_iterator();

		if (begin == end)
		{
			sq_pushnull(vm);
			return 1;
		}

		Sqrat::Array result(SqModule::vm);
		for (auto i = begin; i != end; ++i)
		{
			const std::cmatch& match = *i;
			Sqrat::Table entry(SqModule::vm);

			entry.SetValue("begin", match.position());
			entry.SetValue("end", match.position() + match.length());

			result.Append(entry);
		}

		sq_pushobject(vm, result.GetObject());

		return 1;
	}

	SQBool Regex::match(const char* str) const
	{
		const std::string text(str, std::strlen(str));
		return std::regex_search(text, reg);
	}

	SQRESULT Regex::sq_search(HSQUIRRELVM vm)
	{
		const auto top = sq_gettop(vm);
		if (top > 3)
			return sq_throwerror(vm, "wrong number of parameters");

		if (!Sqrat::ClassType<Regex>::IsValidInstance(vm, 1))
			return sq_throwerror(vm, "invalid this");

		const auto self = Sqrat::ClassType<Regex>::GetInstance(vm, 1);

		const SQChar* str;
		sq_getstring(vm, 2, &str);
		
		SQInteger startIdx = 0;
		if (top == 3) sq_getinteger(vm, 3, &startIdx);

		const auto strLen = std::strlen(str);
		if (static_cast<size_t>(startIdx) >= strLen)
		{
			sq_pushnull(vm);
			return 1;
		}

		const auto begin = std::cregex_iterator(str + startIdx, str + strLen, self->reg);
		const auto end = std::cregex_iterator();
		if (begin == end)
		{
			sq_pushnull(vm);
			return 1;
		}

		Sqrat::Table result(vm);
		const std::cmatch& match = *begin;
		result.SetValue("begin", match.position());
		result.SetValue("end", match.position() + match.length());
		sq_pushobject(vm, result.GetObject());

		return 1;
	}
}
