#include <sqrat.h>

#include "Regex.h"

extern "C" SQRESULT SQRAT_API sqmodule_load(HSQUIRRELVM vm, HSQAPI api)
{
	SqModule::Initialize(vm, api);
	Sqrat::DefaultVM::Set(vm);
	regex::Regex::bind();
	return SQ_OK;
}
