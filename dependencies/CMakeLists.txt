add_subdirectory(squirrel)
add_subdirectory(sqrat)

target_link_libraries(regex PUBLIC Squirrel)
target_link_libraries(regex PUBLIC SqRat)